package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;

public class Kruskal {
    private Graph graph;

    public Kruskal(Graph g) {
        this.graph = g;
    }

    /**
     * fonctions permettant de trier les arretes dans l'ordre croissant ou
     * decroissant
     * 
     * @param g
     * @return un tableau d'arrete Edge[]
     */
    private Edge[] sortedEdges(Graph g, Boolean ascending) {

        Edge[] edges = g.getListEdges().getList().toArray(new Edge[g.getListEdges().getList().size()]).clone();

        Arrays.sort(edges);
        if (!ascending)
            Collections.reverse(Arrays.asList(edges));

        return edges;
    }

    /**
     * verifie si le graphe contient un cycle
     * 
     * @param g
     * @param T
     * @return
     */
    private Boolean checkCycle(Graph tmpGraph, ArrayList<Edge> T) {
        tmpGraph.setListEdges(new ListEdges(T));

        ArrayList<Vertex> listVertices = tmpGraph.getListVertices().getList();
        HashMap<Vertex, Boolean> visitedVertex = new HashMap<Vertex, Boolean>();

        tmpGraph.initializeListAdjacents();

        for (int i = 0; i < listVertices.size(); i++) {
            visitedVertex.put(listVertices.get(i), false);
        }

        Vertex vSearch = tmpGraph.getListEdges().getEdge(tmpGraph.getNbEdges() - 1).getFirstVertex();
        if (checkCycleFromVertex(vSearch, tmpGraph, visitedVertex, vSearch, new Vertex("-1"))) {
            return true;
        }

        return false;
    }

    /**
     * Verifie s'il existe un cycle pour un certain sommet mis en paramètre
     * 
     * @param vertexName
     * @param T
     * @return
     */

    private Boolean checkCycleFromVertex(Vertex vSearch, Graph tmpGraph, HashMap<Vertex, Boolean> visited,
            Vertex curVertex, Vertex parentVertex) {

        if (visited.get(curVertex)) {
            return false;
        }
        visited.replace(curVertex, true);

        ArrayList<Vertex> children = tmpGraph.getListAdjacent().get(curVertex);

        if (children == null) {
            return false;
        }

        for (int i = 0; i < children.size(); i++) {
            if (children.get(i) == vSearch && parentVertex != vSearch) {
                return true;
            }

            if (checkCycleFromVertex(vSearch, tmpGraph, visited, children.get(i), curVertex)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Verifie la connectivité d'un graphe
     * 
     * @param tmpGraph
     * @param T
     * @return
     */
    private Boolean checkConnectivity(Graph tmpGraph, ArrayList<Edge> T) {
        tmpGraph.setListEdges(new ListEdges(T));
        tmpGraph.initializeListAdjacents();

        ArrayList<Vertex> listVertices = tmpGraph.getListVertices().getList();
        HashMap<Vertex, Boolean> visitedVertex = new HashMap<Vertex, Boolean>();

        Vertex vSearch1 = listVertices.get(0);

        for (int k = 0; k < listVertices.size(); k++) {
            visitedVertex.put(listVertices.get(k), false);
        }

        visitChildren(vSearch1, tmpGraph, visitedVertex);

        for (Entry<Vertex, Boolean> b : visitedVertex.entrySet()) {
            if (!b.getValue()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Verifie l'existence d'une chaine entre 2 sommets
     * 
     * @param vFirst
     * @param vLast
     * @param tmpGraph
     * @param visited
     * @return
     */
    private Boolean visitChildren(Vertex vFirst, Graph tmpGraph, HashMap<Vertex, Boolean> visited) {

        if (visited.get(vFirst)) {
            return false;
        }
        visited.replace(vFirst, true);

        ArrayList<Vertex> children = tmpGraph.getListAdjacent().get(vFirst);

        if (children == null) {
            return false;
        }

        for (int i = 0; i < children.size(); i++) {
            visitChildren(children.get(i), tmpGraph, visited);
        }

        return false;
    }

    /**
     * Algoritme d'arbre recouvrant Kruskal1
     * 
     * @return
     */
    public Graph kruskal1() {
        ArrayList<Edge> T = new ArrayList<>();
        Graph g = new Graph();
        g.setListVertices(this.graph.getListVertices());

        Edge[] sortedEdges = sortedEdges(this.graph, true);

        int i = 0;

        while (T.size() < g.getNbVertices() - 1) {
            T.add(sortedEdges[i]);

            if (checkCycle(g, T)) {
                T.remove(sortedEdges[i]);
            }

            i++;
        }

        g.setListEdges(new ListEdges(T));
        g.initializeListAdjacents();
        return g;
    }

    /**
     * Algoritme d'arbre recouvrant Kruskal2
     * 
     * @return
     */
    public Graph kruskal2() {
        ArrayList<Edge> T = new ArrayList<>();
        ArrayList<Edge> tmp = new ArrayList<>();
        tmp = this.graph.getListEdges().getList();
        T = (ArrayList<Edge>) tmp.clone(); 
        // ces lignes sont primordiales pour éviter de changer les valeurs de this.graphe 
        //car cela posera problème si on veut appliquer d'autres algorithmes à ce graphe
        // c'est pourquoi on passe par un clone()

        Graph g = new Graph();
        g.setListVertices(this.graph.getListVertices());

        Edge[] sortedEdges = sortedEdges(this.graph, false);

        int i = 0;

        while (T.size() >= g.getNbVertices() && i < sortedEdges.length) {
            T.remove(sortedEdges[i]);
            if (!checkConnectivity(g, T)) {
                T.add(sortedEdges[i]);
            }
            i++;
        }
        g.setListEdges(new ListEdges(T));
        g.initializeListAdjacents();
        return g;
    }

    /**
     * Algoritme d'arbre recouvrant de type d-MST basé sur kruskal1
     * 
     * @param dMax
     * @return
     */
    public Graph kruskal1_DMST(int dMax) {
        ArrayList<Edge> T = new ArrayList<>();
        Graph g = new Graph();
        g.setListVertices(this.graph.getListVertices());

        Edge[] sortedEdges = sortedEdges(this.graph, true);

        int i = 0;

        while (T.size() < g.getNbVertices() - 1) {

            T.add(sortedEdges[i]);

            if (checkCycle(g, T) || sortedEdges[i].getFirstVertex().getDegree() > dMax
                    || sortedEdges[i].getSecondVertex().getDegree() > dMax) {
                T.remove(sortedEdges[i]);
            }
            i++;
        }

        g.setListEdges(new ListEdges(T));
        g.initializeListAdjacents();
        return g;
    }

}
