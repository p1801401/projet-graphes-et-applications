package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.io.*;
import java.util.Scanner;
import java.util.Map.Entry;

public class Graph {
    private String name;
    private Boolean directed;
    private int nbVertices;
    private int nbEdges;
    private int nbVertexValues;
    private int nbEdgeValues;
    private ListVertices listVertices;
    private ListEdges listEdges;
    private HashMap<Vertex, ArrayList<Vertex>> listAdjacent;

    public Graph() {
        this.name = "";
        this.directed = false;
        this.nbVertices = 0;
        this.nbEdges = 0;
        this.nbVertexValues = 0;
        this.nbEdgeValues = 0;
        this.listVertices = new ListVertices();
        this.listEdges = new ListEdges();
        this.listAdjacent = new HashMap<>();
    }

    public Graph(Graph g) {
        this.name = g.getName();
        this.directed = g.isDirected();
        this.nbVertices = g.getNbVertices();
        this.nbEdges = g.getNbEdges();
        this.nbVertexValues = g.getNbVertexValues();
        this.nbEdgeValues = g.getNbEdgeValues();
        this.listVertices = g.getListVertices();
        this.listEdges = g.getListEdges();
        this.listAdjacent = g.getListAdjacent();
    }

    /**
     * Fonction permettant de charger un graphe depuis un fichier
     * 
     * @param fileName
     */
    public void load(String fileName) {
        int val;
        String mot, nameF, nameS;
        // String separator = System.lineSeparator();
        File graphFile = new File("./assets/" + fileName);
        System.out.println("Loading...");
        try {
            // System.out.println(graphFile.getAbsolutePath());
            FileReader fileReader = new FileReader(graphFile);
            Scanner sr = new Scanner(new BufferedReader(fileReader));
            sr.useDelimiter(" |\\n");

            // DEBUT FICHIER
            sr.next();
            this.name = sr.next();
            sr.next();
            mot = sr.next();
            this.directed = (mot.equals("non")) ? false : true; // MARCHE PAS ENCORE
            sr.next();
            this.nbVertices = sr.nextInt();
            sr.next();
            this.nbVertexValues = sr.nextInt();
            sr.next();
            this.nbEdges = sr.nextInt();
            sr.next();
            this.nbEdgeValues = sr.nextInt();
            sr.next();
            sr.next();
            sr.next();
            sr.next();

            // DEBUT LISTE SOMMETS
            while (sr.hasNextInt()) {
                this.listVertices.addVertex(sr.nextInt());
                sr.nextInt();
            }

            sr.next();
            sr.next();
            sr.next();
            sr.next();

            // DEBUT LISTE ARCS
            while (sr.hasNextInt()) {
                nameF = sr.next();
                nameS = sr.next();
                val = sr.nextInt();
                // this.listEdges.addEdge(new Vertex(nameF), new Vertex(nameS), val);
                this.listEdges.addEdge(this.listVertices.getVertex(nameF), this.listVertices.getVertex(nameS), val);
            }
            sr.close();

            System.out.println("Success load!");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        initializeListAdjacents();
    }

    public String getName() {
        return this.name;
    }

    public Boolean isDirected() {
        return this.directed;
    }

    public int getNbVertices() {
        if (this.nbVertices == 0) {
            this.nbVertices = this.listVertices.getList().size();
        }
        return this.nbVertices;
    }

    public int getNbEdges() {
        this.nbEdges = this.listEdges.getList().size();
        return this.nbEdges;
    }

    public int getNbVertexValues() {
        return this.nbVertexValues;
    }

    public int getNbEdgeValues() {
        return this.nbEdgeValues;
    }

    public ListEdges getListEdges() {
        return this.listEdges;
    }

    public HashMap<Vertex, ArrayList<Vertex>> getListAdjacent() {
        return listAdjacent;
    }

    public ListVertices getListVertices() {
        return this.listVertices;
    }

    public void setListVertices(ListVertices listVertices) {
        this.listVertices = listVertices;
        this.nbVertices = listVertices.getList().size();
    }

    public void setListEdges(ListEdges listEdges) {
        this.listEdges = listEdges;
        this.nbEdges = listEdges.getList().size();
    }


    public void setName(String name) {
        this.name = name;
    }

    /**
     * Permet de générer la liste d'adjacence à partir des listes de sommets et
     * d'arrêtes préalablement set
     */
    public void initializeListAdjacents() {
        ArrayList<Vertex> lVertexs = this.listVertices.getList();
        ArrayList<Edge> lEdge = this.listEdges.getList();

        for (int i = 0; i < lVertexs.size(); i++) {
            ArrayList<Vertex> a = new ArrayList<>();
            for (int j = 0; j < lEdge.size(); j++) {

                if (lEdge.get(j).getFirstVertex().getName().equals(lVertexs.get(i).getName())) {
                    a.add(lEdge.get(j).getSecondVertex());
                }

                if (lEdge.get(j).getSecondVertex().getName().equals(lVertexs.get(i).getName())) {
                    a.add(lEdge.get(j).getFirstVertex());
                }

            }

            this.listAdjacent.put(lVertexs.get(i), a);
            lVertexs.get(i).setDegree(a.size());
        }
    }

    /**
     * Permet de générer la liste d'adjacence à partir des listes de sommets et
     * d'arrêtes entrées en paramètre
     * 
     * @param lVertex
     * @param lEdges
     * @return
     */
    public HashMap<Vertex, ArrayList<Vertex>> createListAdjacent(ListEdges lVertex, ListEdges lEdges) {

        ArrayList<Vertex> lVertexs = this.listVertices.getList();
        ArrayList<Edge> lEdge = this.listEdges.getList();

        HashMap<Vertex, ArrayList<Vertex>> lAdjacents = new HashMap<>();

        for (int i = 0; i < lVertexs.size(); i++) {
            ArrayList<Vertex> a = new ArrayList<>();
            for (int j = 0; j < lEdge.size(); j++) {
                if (lEdge.get(j).getFirstVertex() == lVertexs.get(i)) {
                    a.add(lEdge.get(j).getSecondVertex());
                }

                if (lEdge.get(j).getSecondVertex() == lVertexs.get(i)) {
                    a.add(lEdge.get(j).getFirstVertex());
                }

            }
            lAdjacents.put(lVertexs.get(i), a);
            lVertexs.get(i).setDegree(a.size());
        }

        return lAdjacents;
    }

    public void unsetListEdges() {
        this.listEdges = new ListEdges();
    }

    /**
     * Afficher un graphe
     */
    public void printGraph() {

        System.out.println("Nom : " + getName());
        System.out.println("Graphe oriente : " + isDirected());
        System.out.println("Nombre de sommets : " + getNbVertices());

        for (Entry<Vertex, ArrayList<Vertex>> v : this.listAdjacent.entrySet()) {
            int nbChild = v.getValue() == null ? 0 : v.getValue().size();
            System.out.println("Vertex : " + v.getKey().getName() + "  (" + nbChild + " children)");
            for (int i = 0; i < v.getValue().size(); i++) {
                System.out.println("-> " + v.getValue().get(i).getName());
            }
        }
    }

    public int getWeight() {
        return this.listEdges.getValue();
    }

    public int getDegree() {
        int max = 0;
        for (Vertex vertex : this.listVertices.getList()) {
            max = max > vertex.getDegree() ? vertex.getDegree() : max;
        }
        return max;
    }

    /**
     * Permet de verifier la connectivité d'un graphe
     * @return
     */

    public Boolean checkConnectivity() {

        HashMap<Vertex, Boolean> visitedVertex = new HashMap<Vertex, Boolean>();
        

        for (int i = 0; i < this.listVertices.getList().size(); i++) {
            visitedVertex.put(this.listVertices.getList().get(i), false);
        }

        Vertex vSearch1 = this.listVertices.getList().get(0);
        for (int j = 1; j < this.listVertices.getList().size(); j++) {

            Vertex vSearch2 = this.listVertices.getList().get(j);

            if(!checkChainBetweenVertices(vSearch1, vSearch2,visitedVertex)){
                return false;
            }
            for (int k = 0; k < this.listVertices.getList().size(); k++) {
                visitedVertex.put(this.listVertices.getList().get(k), false); 
            }
            
        }

        return true;
    }

    /**
     * Verifie s'il exite une chaine entre 2 vertex
     * @param vFirst
     * @param vLast
     * @param visited
     * @return
     */
    private Boolean checkChainBetweenVertices(Vertex vFirst, Vertex vLast, HashMap<Vertex, Boolean> visited) {
        
        if (visited.get(vFirst)){
            return false;
        }
        visited.replace(vFirst, true);

        ArrayList<Vertex> children = this.listAdjacent.get(vFirst);

        if(children == null){
            return false;
        }
         
        for (int i = 0; i < children.size(); i++){
            if(children.get(i) == vLast){
                return true;
            }
            
            if(checkChainBetweenVertices(children.get(i), vLast, visited)){
                return true;
            }
        }
        return false;
    }
}
