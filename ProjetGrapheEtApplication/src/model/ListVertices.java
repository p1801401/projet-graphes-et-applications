package model;

import java.util.ArrayList;

public class ListVertices {
    private ArrayList<Vertex> list;

    public ArrayList<Vertex> getList() {
        return this.list;
    }

    public ListVertices() {
        this.list = new ArrayList<>();
    }

    public void addVertex(int n) {
        String name = Integer.toString(n);
        Vertex v = new Vertex(name);
        this.list.add(v);
    }

    public void addVertex(Vertex v) {
        this.list.add(v);
    }

    /**
     * Get un Sommet à partir de son nom
     * @param name
     * @return
     */
    public Vertex getVertex(String name) {
        for (int i = 0; i < this.list.size(); i++) {
            if(this.list.get(i).getName().equals(name)){
                return this.list.get(i); 
            }
        }
        return null;
    }
}
