package model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;

public class Prim {
    private Graph graph;

    public Prim(Graph g) {
        this.graph = g; 
    }

    /**
     * fonctions permettant de trier les arretes dans l'ordre croissant
     * @param g
     * @return un tableau d'arrete 
     */
    private ArrayList<Edge> sortedEdges(Graph g){

        //obligé d'utiliser la fonction clone pour que lorsqu'on modifie sE, on ne modifie pas la liste d'arcs dans le graphe

        ArrayList<Edge> edges = (ArrayList<Edge>) g.getListEdges().getList().clone(); 

        edges.sort(Comparator.naturalOrder());

        return edges; 
    }


    private Boolean checkEquals(ArrayList<Vertex> X, ArrayList<Vertex> R) {
        for (int i = 0; i < X.size(); i++) {
            if(!R.contains(X.get(i))){
                return false;
            }
        }
        return true;
    }

    /**
     * Fonction permettant de trouver une arrête de poids minimum respectant les conditions de l'algorithme Prim
     * @param sortedEdges
     * @param R
     * @return
     */
    private Edge findMinimumEdge(ArrayList<Edge> sortedEdges, ArrayList<Vertex> R){
        Edge e; 
        for (int i = 0; i < sortedEdges.size(); i++) {
            if(R.contains(sortedEdges.get(i).getFirstVertex()) && !R.contains(sortedEdges.get(i).getSecondVertex())){
                
                e = sortedEdges.get(i); 
                sortedEdges.remove(i);
                return e; 
            }
            else if(R.contains(sortedEdges.get(i).getSecondVertex()) && !R.contains(sortedEdges.get(i).getFirstVertex())){
               
                e = sortedEdges.get(i);
                sortedEdges.remove(i);
                return e;  
            }
        }

        System.err.println("Aucun minimum a ete trouvee");
        return null; 
    }

    /**
     * Algorithme d'arbre recouvrant Prim
     * @return
     */
    public Graph prim() {
        Graph g = new Graph();
        g.setListVertices(this.graph.getListVertices());

        ArrayList<Edge> T = new ArrayList<>(); 
        ArrayList<Vertex> R = new ArrayList<>(); 

        ArrayList<Edge> sortedEdges = sortedEdges(this.graph);
        Vertex x = this.graph.getListVertices().getList().get(new Random().nextInt(this.graph.getNbVertices())); 
        R.add(x);  

        while (!checkEquals(this.graph.getListVertices().getList(), R)) {
            Edge e = findMinimumEdge(sortedEdges, R); 
            T.add(e); 
            if(R.contains(e.getSecondVertex())){
                R.add(e.getFirstVertex());  
            }
            else{
                R.add(e.getSecondVertex());
            }
        }
 
        g.setListEdges(new ListEdges(T));
        g.initializeListAdjacents();

        return g; 
    }

    /**
     * Algoritme d'arbre recouvrant de type d-MST basé sur prim
     * @param dMax
     * @return
     */
    public Graph prim_DMST(int dMax) {
        Graph g = new Graph();
        g.setListVertices(this.graph.getListVertices());

        ArrayList<Edge> T = new ArrayList<>(); 
        ArrayList<Vertex> R = new ArrayList<>(); 

        ArrayList<Edge> sortedEdges = sortedEdges(this.graph);
        Vertex x = this.graph.getListVertices().getList().get(new Random().nextInt(this.graph.getNbVertices())); 
        R.add(x);  

        while (!checkEquals(this.graph.getListVertices().getList(), R)) {
            Edge e = findMinimumEdge(sortedEdges, R); 
            T.add(e); 

            g.setListEdges(new ListEdges(T));
            g.initializeListAdjacents();

            if(R.contains(e.getSecondVertex())){
                R.add(e.getFirstVertex());  
            }
            else{
                R.add(e.getSecondVertex());
            }
            if(e.getFirstVertex().getDegree() > dMax || e.getSecondVertex().getDegree() > dMax){
                T.remove(T.size()-1); 
                R.remove(R.size()-1); 
            }
        }
 
        g.setListEdges(new ListEdges(T));
        g.initializeListAdjacents();

        return g; 
    }
}
