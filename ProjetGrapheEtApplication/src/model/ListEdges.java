package model;

import java.util.ArrayList;

public class ListEdges {
    private ArrayList<Edge> list;
    private int value;

    public ListEdges(ArrayList<Edge> lEdges){
        this.list = lEdges;
        if(lEdges != null) {
            for(int i = 0; i < lEdges.size(); i++) {
                this.value += lEdges.get(i).getValue();
            }
        }
    }

    public ListEdges() {
        this(new ArrayList<>());
    }

    public Edge getEdge(int indice) {
        return this.list.get(indice);
    }

    public ArrayList<Edge> getList() {
        return this.list;
    }

    public int getValue() {
        return this.value;
    }

    /**
     * Supprime un Edge de la liste
     * @param indice
     */
    public void removeEdge(int indice) {
        this.value -= this.list.get(indice).getValue();
        this.list.remove(indice);
    }

    /**
     * Crée un Edge et l'ajoute dans la liste 
     * @param vF
     * @param vS
     * @param val
     */
    public void addEdge(Vertex vF, Vertex vS, int val) {
        Edge e = new Edge(vF, vS, val); 
        this.list.add(e);
        this.value += val;
    }
}
