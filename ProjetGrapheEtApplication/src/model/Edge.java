package model;

public class Edge implements Comparable<Edge>{
    private Vertex firstVertex;
    private Vertex secondVertex;
    private int value;

    public Edge(){
        
    }

    public int getValue() {
        return value;
    }

    /**
     * Fonction necessaire à l'interface Comparable 
     */
    @Override
    public int compareTo(Edge e) {
        int compareValue = e.getValue();

        //ordre croissant 
        return this.value - compareValue;

        //ordre décroissant
        //return compareValue - this.value;
    }

    /**
     * Constructeur de Edge à partir des nom des Vertex (String) 
     * @param nameF
     * @param nameS
     * @param val
     */
    public Edge(Vertex nameF, Vertex nameS, int val) {
        this.firstVertex = nameF;
        this.secondVertex = nameS;
        this.value = val;
    }

    public Vertex getFirstVertex() {
        return firstVertex;
    }

    public Vertex getSecondVertex() {
        return secondVertex;
    }
}
