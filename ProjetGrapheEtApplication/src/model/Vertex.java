package model;

public class Vertex {
    private String name;
    private int degree;

    public Vertex(){
        this.name = "-1"; 
        this.degree = -1; 
    }

    public Vertex(String name) {
        this.name = name;
        this.degree = 0;   
    }

    public int getDegree() {
        return this.degree;
    }

    public String getName() {
        return this.name;
    }

    public void setDegree(int degree) {
        this.degree = degree;
    }
}
