import model.*;
import java.util.Random;

public class App {

    public Graph graphExample() {

        Graph g = new Graph();
        g.setName("Exemple");

        Vertex v1 = new Vertex("1");
        Vertex v2 = new Vertex("2");
        Vertex v3 = new Vertex("3");
        Vertex v4 = new Vertex("4");
        Vertex v5 = new Vertex("5");
        Vertex v6 = new Vertex("6");
        Vertex v7 = new Vertex("7");
        Vertex v8 = new Vertex("8");

        ListVertices listVertices = new ListVertices();
        listVertices.addVertex(v1);
        listVertices.addVertex(v2);
        listVertices.addVertex(v3);
        listVertices.addVertex(v4);
        listVertices.addVertex(v5);
        listVertices.addVertex(v6);
        listVertices.addVertex(v7);
        listVertices.addVertex(v8);

        ListEdges listEdges = new ListEdges();
        listEdges.addEdge(v1, v2, 2);
        listEdges.addEdge(v2, v3, 5);
        listEdges.addEdge(v3, v4, 3);
        listEdges.addEdge(v3, v1, 4);
        listEdges.addEdge(v4, v5, 1);
        listEdges.addEdge(v1, v5, 10);
        listEdges.addEdge(v6, v1, 11);
        listEdges.addEdge(v7, v5, 7);
        listEdges.addEdge(v8, v2, 9);
        listEdges.addEdge(v8, v3, 0);
        listEdges.addEdge(v6, v7, 12);
        listEdges.addEdge(v5, v6, 14);
        listEdges.addEdge(v2, v4, 6);
        listEdges.addEdge(v7, v3, 19);

        g.setListVertices(listVertices);
        g.setListEdges(listEdges);
        g.initializeListAdjacents();

        return g;
    }

    public void testKruskal1(Graph g) {
        Kruskal k = new Kruskal(g);

        System.out.println();
        System.out.println();
        System.out.println();

        Chrono c = new Chrono();
        System.out.println("Kruskal 1 :");

        c.start();

        Graph kg = k.kruskal1();

        c.stop();

        kg.setName(g.getName());
        kg.printGraph();

        System.out.println("Resultat conforme : " + kg.checkConnectivity());
        System.out.println("Duree K1 : " + c.getDureeMs() + " ms");
        System.out.println(" Kruskal1 weight : " + kg.getWeight());
        System.out.println();
    }

    public void testKruskal2(Graph g) {
        Kruskal k = new Kruskal(g);

        System.out.println();
        System.out.println();
        System.out.println();

        Chrono c = new Chrono();
        System.out.println("Kruskal 2 :");
        c.start();

        Graph kg = k.kruskal2();

        c.stop();

        kg.setName(g.getName());
        kg.printGraph();
        System.out.println("Resultat conforme : " + kg.checkConnectivity());
        System.out.println("Duree K2 : " + c.getDureeMs() + " ms");
        System.out.println(" Kruskal2 weight : " + kg.getWeight());
        System.out.println();
    }

    public void testKruskal1_DMST(Graph g, int dMax) {

        Kruskal k = new Kruskal(g);

        System.out.println();
        System.out.println();
        System.out.println();

        Chrono c = new Chrono();
        System.out.println("Kruskal d-MST :");
        c.start();

        Graph kg = k.kruskal1_DMST(dMax);

        c.stop();

        if (kg != null) {

            kg.setName(g.getName());
            kg.printGraph();
            System.out.println("Resultat conforme : " + kg.checkConnectivity());
            System.out.println(" Kruskal1 d-MST weight : " + kg.getWeight());
        }
        System.out.println("Duree K d-MST (d = " + dMax + ") : " + c.getDureeMs() + " ms");
        System.out.println();

    }

    public void testPrim(Graph g) {
        Prim p = new Prim(g);

        System.out.println();
        System.out.println();
        System.out.println();

        Chrono c = new Chrono();
        System.out.println("Prim:");
        c.start();

        Graph pg = p.prim();

        c.stop();

        pg.setName(g.getName());
        pg.printGraph();
        System.out.println("Resultat conforme : " + pg.checkConnectivity());
        System.out.println("Duree Prim : " + c.getDureeMs() + " ms");
        System.out.println(" Prim weight : " + pg.getWeight());
        System.out.println();
    }

    public void testPrim_DMST(Graph g, int dMax) {
        Prim p = new Prim(g);

        System.out.println();
        System.out.println();
        System.out.println();

        Chrono c = new Chrono();
        c.start();

        System.out.println("Prim d-MST:");
        Graph pg = p.prim_DMST(dMax);
        pg.printGraph();

        c.stop();
        System.out.println("Resultat conforme : " + pg.checkConnectivity());
        System.out.println("Duree Prim d-MST  (d = " + dMax + ") : " + c.getDureeMs() + " ms");
        System.out.println(" Prim weight : " + pg.getWeight());
        System.out.println();
    }

    public Graph getRandomGraph() {
        String[] filenames = { "crd300.gsb", "crd500.gsb", "crd700.gsb", "crd1000.gsb", "shrd150.gsb", "shrd200.gsb",
                "shrd300.gsb", "str300.gsb", "str500.gsb", "str700.gsb", "str1000.gsb", "sym300.gsb", "sym500.gsb",
                "sym700.gsb" };
        String randFile = filenames[new Random().nextInt(filenames.length)];
        Graph g = new Graph();
        g.load(randFile);
        return g;
    }

    public void testDiffKruskal(Graph g){
        Kruskal k = new Kruskal(g);

        for (int i = 2; i < 5; i++) {
            Graph mst = k.kruskal1();   
            Graph dmst = k.kruskal1_DMST(i);

            System.out.println("Resultat conforme kruskal mst : " + mst.checkConnectivity());
            System.out.println("Resultat conforme kruskal dmst (d = "+ i +"): " + dmst.checkConnectivity());

            System.out.println(" Poids kruskal mst : " + mst.getWeight());
            System.out.println(" Poids kruskal dmst (d = "+ i +"): " + dmst.getWeight());
            Double rapport = (double) dmst.getWeight()/ (double)mst.getWeight();
            System.out.println(" Rapport de poids dmst/mst : " + rapport);
            System.out.println();
        }

    }

    public void testDiffPrim(Graph g){
        Prim p = new Prim(g);

        for (int i = 2; i < 5; i++) {
            Graph mst = p.prim();   
            Graph dmst = p.prim_DMST(i);

            System.out.println("Resultat conforme prim mst : " + mst.checkConnectivity());
            System.out.println("Resultat conforme prim dmst (d = "+ i +"): " + dmst.checkConnectivity());

            System.out.println(" Poids prim mst : " + mst.getWeight());
            System.out.println(" Poids prim dmst (d = "+ i +"): " + dmst.getWeight());
            Double rapport = (double) dmst.getWeight()/ (double)mst.getWeight();
            System.out.println(" Rapport de poids dmst/mst : " + rapport);
            System.out.println();
        }

    }

    public static void main(String[] args) throws Exception {
        App app = new App();


        // Sur un petit graphe

        Graph g = app.graphExample();

        // g.printGraph();
        // app.testKruskal1(g);
        // app.testKruskal2(g);

        // app.testPrim(g);
        
        // app.testKruskal1_DMST(g, 2);
        // app.testPrim_DMST(g, 2);

        // Sur un graphe random 

        Graph g2 = app.getRandomGraph();
        app.testKruskal1(g2);
        
        app.testPrim(g2);
        app.testKruskal2(g2);
        
        app.testKruskal1_DMST(g2, 3);
        app.testPrim_DMST(g2, 3);
        
        app.testDiffKruskal(g2);
        app.testDiffPrim(g2);

    }
}
